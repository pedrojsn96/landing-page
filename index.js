import Hero from '../components/Hero'
import HowItWorks from "../components/HowItWorks"
import Navbar from "../components/Navbar"
import React from "react"
import SEO from "../components/Seo"
import Why from "../components/Why"

const IndexPage = () => (
 
    <SEO />
    <Navbar />
    <Hero />
    <Why />
    <HowItWorks />

)

export default IndexPage
