import { Container, Typography } from '@material-ui/core';

import React from 'react';
import SEO from '../components/Seo';

const NotFoundPage = () => (
  <Container>
    <SEO title="404: Not found" />
    <Typography variant="h1">Page not Found :(</Typography>
    <Typography variant="body1">You just hit a route that doesn&#39;t exist... the sadness.</Typography>
  </Container>
);

export default NotFoundPage;
