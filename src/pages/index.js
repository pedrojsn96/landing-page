import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import Hero from '../components/Hero';
import HowItWorks from '../components/HowItWorks';
import Navbar from '../components/Navbar';
import React from 'react';
import SEO from '../components/Seo';
import Trouble from '../components/Trouble';
import Why from '../components/Why';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Open sans',
  },
  overrides: {
    MuiTypography: {
      subtitle1: {
        margin: '10px 0',
      },
      subtitle2: {
        margin: '10px 0',
      },
      button: {
        fontSize: '1.3em',
      },
    },
    MuiButtonBase: {
      root: {
        minHeight: '56px',
      },
    },
  },
});
const IndexPage = () => (
  <ThemeProvider theme={theme}>
    <SEO />
    <Navbar />
    <Hero />
    <Trouble />
    <Why />
    <HowItWorks />
  </ThemeProvider>
);

export default IndexPage;
