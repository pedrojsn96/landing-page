import './index.scss';

import { Container, Typography } from '@material-ui/core';
import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import addToMailchimp from 'gatsby-plugin-mailchimp';
import image from '../../images/feature.png';

const Hero = () => {
  const [email, setEmail] = useState('');
  const [response, setResponse] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    addToMailchimp(email).then((data) => {
      setResponse(data);
    }).catch(() => {});
  };

  const handleChange = (event) => {
    setEmail(event.target.value);
  };

  return (
    <Container className="hero">
      <div className="hero__container">
        <div className="hero__content">
          <Typography variant="h1">Realize trocas sem stress</Typography>
          <Typography variant="subtitle1">É comum comprarmos objetos para suprir necessidades momentâneas, porém nossas necessidades mudam. Já pensou nos novos objetos para suas novas necessidades? Bem, nós podemos ajudar você.</Typography>
        </div>
        <div className="hero__image-container">
          <img className="hero__image" alt="hero" src={image} />
        </div>
      </div>
      <form onSubmit={handleSubmit} className="hero__form">
        <TextField
          id="hero-email-input"
          label="Email"
          type="email"
          name="email"
          autoComplete="email"
          variant="outlined"
          onChange={handleChange}
        />
        <br />
        <Button
          variant="contained"
          color="primary"
          label="Submit"
          type="submit"
        >
          <Typography variant="button">Insira seu e-mail e receba um convite</Typography>
        </Button>
      </form>
    </Container>
  );
};

export default Hero;
