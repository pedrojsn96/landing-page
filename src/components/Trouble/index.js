import './index.scss';

import { Container, Typography } from '@material-ui/core';

import React from 'react';
import image from '../../images/feature.png';

const Trouble = () => (
  <Container>
    <div className="trouble">
      <div className="trouble__content">
        <Typography variant="h2" className="trouble__title">Trocar pode ser estressante</Typography>
        <Typography variant="subtitle1" className="trouble__description">Nós sabemos que trocar pode ser bem difícil, especialmente quando utilizamos alguma plataforma cujo o objetivo principal não está na realização de trocas</Typography>
      </div>
      <img className="trouble__image" alt="trouble" src={image} />
    </div>
  </Container>
);

export default Trouble;
