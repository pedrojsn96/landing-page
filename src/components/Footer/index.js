import './index.scss';

import { Container, Link, Typography } from '@material-ui/core';

import PropTypes from 'prop-types';
import React from 'react';

const Footer = ({ siteTitle }) => {
  const preventDefault = (event) => event.preventDefault();
  return (
    <footer className="footer">
      <Container>
        <div className="footer__content">
          <div className="footer__contact">
            <Typography className="footer__link" variant="overline">
              <Link href="#" onClick={preventDefault}>
                Contact Us
              </Link>
            </Typography>
          </div>
          <div className="footer__date">
            <Typography variant="overline">
              {`${new Date().getFullYear()} ${siteTitle}`}
            </Typography>
          </div>
        </div>
      </Container>
    </footer>
  );
};

Footer.propTypes = {
  siteTitle: PropTypes.string,
};

Footer.defaultProps = {
  siteTitle: '',
};

export default Footer;
