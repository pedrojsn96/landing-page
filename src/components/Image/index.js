import { StaticQuery, graphql } from 'gatsby';

import Img from 'gatsby-image';
import React from 'react';

const Image = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(relativePath: { eq: "feature.png" }) {
          childImageSharp {
            fluid(maxWidth: 300) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={(data) => <Img fluid={data.placeholderImage.childImageSharp.fluid} />}
  />
);
export default Image;
