import './index.scss';

import { Container, Typography } from '@material-ui/core';

import React from 'react';
import cadastro from '../../images/cadastrar.svg';
import colocar from '../../images/colocar.svg';
import procurar from '../../images/procurar.svg';
import trocar from '../../images/matching2.svg';

const HowItWorks = () => (
  <Container className="how-it-works">
    <Typography variant="h2" className="how-it-works__title">Veja como funciona</Typography>
    <Typography variant="subtitle1" className="how-it-works__description">Rápido, fácil e sem burocracia</Typography>
    <ol className="how-it-works__steps">
      <li className="how-it-works__step">
        <img className="how-it-works__step-image" alt="item" src={cadastro} />
        <Typography variant="body1" className="how-it-works__step-description">Cadastre os objetos que deseja trocar</Typography>
      </li>
      <li className="how-it-works__step">
        <img className="how-it-works__step-image" alt="item" src={colocar} />
        <Typography variant="body1" className="how-it-works__step-description">Informe qual objeto está precisando</Typography>
      </li>
      <li className="how-it-works__step">
        <img className="how-it-works__step-image" alt="item" src={procurar} />
        <Typography variant="body1" className="how-it-works__step-description">Iremos procurar o match perfeito para você</Typography>
      </li>
      <li className="how-it-works__step">
        <img className="how-it-works__step-image" alt="item" src={trocar} />
        <Typography variant="body1" className="how-it-works__step-description">Realize a troca</Typography>
      </li>
    </ol>
  </Container>
);

export default HowItWorks;
