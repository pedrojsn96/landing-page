import './index.scss';

import { Container, Typography } from '@material-ui/core';

import React from 'react';
import why from '../../images/feature.png';

const Why = () => (
  <div className="why">
    <Container>
      <Typography variant="h2" className="why__title">Por isso criamos uma plataforma que conecta pessoas e seus objetos</Typography>
      <img className="why__image" alt="why" src={why} />
    </Container>
  </div>
);

export default Why;
