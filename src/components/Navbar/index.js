import './index.scss';

import { Container } from '@material-ui/core';
import React from 'react';
import logo from '../../images/logo.jpeg';

const Navbar = () => (
  <nav className="navbar">
    <Container>
      <div className="navbar__logo">
        <img className="navbar__image" alt="logo" src={logo} />
      </div>
    </Container>
  </nav>
);

export default Navbar;
